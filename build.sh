#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usage:"
    echo  $0 "[platform] [action]"
    echo "i.e."
    echo $0 arm64
    echo $0 arm32 push
    echo "platform: arm64 arm32 amd64 - for raspberry PI 32bit, 64 bit or VM/desktop"
    echo "action: build push - to build docker image or to push to registry"
    echo "default action is build, push is additional to build"
    echo "to build without buildx for current platform use:"
    echo "docker build -t registry.gitlab.com/rndan/rpi-apps/gps ."
    exit
fi
echo $0 $1 $2

if [ "$1" != "arm64" ] && [ "$1" != "arm32" ] && [ "$1" != "amd64" ]
then
  echo "$1 - not supported platform"
  exit
fi

if [ "$2" == "push" ]
then
  echo "do not forget to \ndocker login registry.gitlab.com" 
  #docker push registry.gitlab.com/rndan/rpi-apps/acc
fi
REGISTRY="registry.gitlab.com"
RNDAN="/rndan/rpi-apps/"
APPNAME="gps"

if [ "$1" == "arm64" ]
then
  HWARCH=rpi64
  IMAGE=${REGISTRY}${RNDAN}${APPNAME}
  IMAGETAG=${IMAGE}:${HWARCH}
  PLATFORM="linux/arm64"
  echo "buildx build --platform $PLATFORM -t $IMAGETAG ."
  docker buildx build --platform $PLATFORM -t $IMAGETAG .
  if [ "$2" == "push" ]
  then
    echo "push $IMAGETAG"
    docker push $IMAGETAG
  fi
fi

if [ "$1" == "arm32" ]
then
  HWARCH=rpi32
  IMAGE=${REGISTRY}${RNDAN}${APPNAME}
  IMAGETAG=${IMAGE}:${HWARCH}
  PLATFORM="linux/arm/v7"
  echo "buildx build --platform $PLATFORM -t $IMAGETAG ."
  docker buildx build --platform $PLATFORM -t $IMAGETAG .
  if [ "$2" == "push" ]
  then
    echo "push $IMAGETAG"
    docker push $IMAGETAG
  fi
fi

if [ "$1" == "amd64" ]
then
  HWARCH=amd64
  IMAGE=${REGISTRY}${RNDAN}${APPNAME}
  IMAGETAG=${IMAGE}:${HWARCH}
  PLATFORM="linux/amd64"
  echo "buildx build --platform $PLATFORM -t $IMAGETAG ."
  docker buildx build --platform $PLATFORM -t $IMAGETAG .
  if [ "$2" == "push" ]
    then
    echo "push $IMAGETAG"
    docker push $IMAGETAG
  fi
fi
