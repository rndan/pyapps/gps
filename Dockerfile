# FROM ubuntu:22.04
# for RPI 32 bit:
# FROM ubuntu:18.04

# RUN apt-get update && apt install -y python3-pip python3-netifaces && rm -rf /usr/lib/python3.*/EXTERNALLY-MANAGED && pip3 install "paho-mqtt<2.0.0" 
# if fails try pip install without flag --break-system-packages
# RUN apt update && apt install -y python3-pip python3-netifaces && pip3 install "paho-mqtt<2.0.0" --break-system-packages

FROM alpine
RUN apk update && apk add py3-pip py3-netifaces && rm -rf /usr/lib/python3.*/EXTERNALLY-MANAGED && pip3 install "paho-mqtt<2.0.0"

ENV BROKER=broker.emqx.io
ENV TOPIC=in
ENV DPORT=5000
ENV DTYPE=data
ENV PROTO=udp
ENV DEVID=rpi0gps

COPY gpspub.py /home/gpspub.py

CMD [ "sh", "-c", "python3 /home/gpspub.py $BROKER $TOPIC $DPORT $DTYPE $PROTO $DEVID"]
