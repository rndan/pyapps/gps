# MQTT Publisher of GPS Navigation data

# Detect RPI CPU architecture
To get architectute tyoe use 'uname -m' or dpkg '--print-architecture' command on RPI:\
armhf or armv7l means 32 bit arm

# Docker install
on RPI 32 bit:\
https://docs.docker.com/engine/install/raspberry-pi-os/ \
on RPI 64bit:\
https://docs.docker.com/engine/install/debian/ \
on x86_64 PC\
https://docs.docker.com/engine/install/ubuntu/

# Install buildx plugin
sudo apt install docker-buildx\
or in raspberry can be\
sudo apt install docker-buildx-plugin

# Install other docker platforms support
sudo apt-get install -y qemu qemu-user-static binfmt-support\
check supported platform list\
docker buildx ls

# Get rid of sudo for docker
sudo groupadd docker\
sudo usermod -aG docker $USER

# Build docker image
Native for i.e. x86_64 image on x86_64 PC  or armv7l image on armv7l RPI\
docker build . -t registry.gitlab.com/rndan/rpi-apps/gps:x64\
Cross build for raspberri 64 bit\
docker buildx build --platform linux/arm64 -t registry.gitlab.com/rndan/rpi-apps/gps:rpi64 .\
Cross build for raspberri 32 bit\
docker buildx build --platform linux/arm/v7 -t registry.gitlab.com/rndan/rpi-apps/gps:rpi32 .\
Note: In case of docker native build due to gpg keys issue with docker in RPI 32 bit need to downgrade ubuntu release in Dockerfile to 18.04

# Push image to registry
For raspberri 64 bit\
docker push registry.gitlab.com/rndan/rpi-apps/gps:rpi64\
for raspberri 32 bit\
docker push registry.gitlab.com/rndan/rpi-apps/gps:rpi32

# Dockerfile args meaning and their default values
MQTT brocker address or server name, i.e. 192.168.0.1 or 127.0.0.1\
BROKER='broker.emqx.io'\
MQTT topic:\
TOPIC=in\
udp incomming port to listen NMEA sentences:\
DPORT=2947\
Type of output data in serialized JSON: data, raw or none\
raw - forwarded raw NMEA sentences wrapped into JSON\
data - exctracted from NMEA sentences values: speed, lattitude, longitude, altitude wrapped into JSON\
none - output JSON without navigation data\
DTYPE=data\
device ID, i.e. rpi0 or it serail number etc.\
DEVID=dummy0
mode of publisher: udp or fake\
in fake mode it generates own dummy data\
if PROTO=udp then upd port is handled to receive and parse incomming data from gps sensor streamer\
PROTO=udp

# Run docker image examples
docker push registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
with overwritten MQTT brocker address and port forwarding:\
docker run -it -e BROKER=192.168.0.107 -e DPORT=5000 -p 2947:5000/udp registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
or RPI image:\
docker run -it -e BROKER=192.168.0.107 -e DPORT=5000 -p 2947:5000/udp registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
or with default args:\
docker run -it -p 2947:2947/udp registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
Note: to avoid some issues with (re)connecting multiple MQTT clients assign different mac addresses\
      with --mac-address parameter:\
docker run -it -e BROKER=192.168.0.107 -p 2947:2947/udp --mac-address 02:42:ac:11:00:04 registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
docker run -it -e BROKER=192.168.0.107 -p 2947:2947/udp --mac-address 02:42:ac:11:00:03 registry.gitlab.com/rndan/rpi-apps/acc:rpi32\
start with fake data\
docker run -it -e BROKER=192.168.0.107 -e DPORT=5000 -e PROTO=fake -p 2947:5000/udp registry.gitlab.com/rndan/rpi-apps/gps:rpi32

# Utils to copy build docker images to RPI
Via registry:\
pull image from registry depending on HW architecture\
for raspberri 64 bit\
docker pull registry.gitlab.com/rndan/rpi-apps/gps:rpi64\
for raspberri 32 bit\
docker pull registry.gitlab.com/rndan/rpi-apps/gps:rpi32\
Or via scp:\
export docker image into tar file\
docker save registry.gitlab.com/rndan/rpi-apps/gps:rpi32 > gpspub.tar\
copy it over network, i.e. scp to RPI and then import as docker image\
docker load -i gpspub.tar\
then run as described above.

# Incomming navigation data 
Navigation gps sensor application data expected to be received on UPD port as NMEA sentences, i.e.:\
$GPGGA,215830.000,5342.4461,N,01474.9595,E,0,00,127.000,417.593,M,0,M,,*75.\
$GPRMC,215830.000,V,5342.4461,N,01474.9595,E,0.000,248.929,050424,,E,N*37.\
$GPGLL,5342.4461,N,01474.9595,E,215830.000,V,N*4E.\
$GPVTG,248.929,T,,M,0.000,N,0.000,K,N*0E.
