# run with python3

import json
import netifaces
import socket
import random
import time
import sys
from paho.mqtt import client as mqtt_client

# to use paho-mqtt 1.X install older version:
# pip3 install "paho-mqtt<2.0.0"
# to use with paho-mqtt 2.X change commented lines with new API  install it
# pip3 install paho-mqtt
# https://eclipse.dev/paho/files/paho.mqtt.python/html/migrations.html

# usage:
# python3 gpspub.py [broker] [topic] [port] [type] [proto] [deviceid]
# python3 gpspub.py 127.0.0.1 in 2947 data fake rpi0
# or
# python3 gpspub.py 192.168.0.107

# default broker, mqtt port, topic, data type, udp port to listen , device ID, proto (configure from file?)
broker = 'broker.emqx.io'
port = 1883
udp = 5000
topic = 'in'
type = 'data'
proto = 'udp' # udp or fake

#global alt, lon, lat
alt = 0.0
lat = 0.0
lon = 0.0

# Generate a Client ID with the publish prefix.
nifs = netifaces.interfaces()
sensId = 'gps'
deviceId = f'publish-{sensId}-{random.randint(0, 1000)}'

def getmac(interface):
    try:
        mac = open('/sys/class/net/'+interface+'/address').readline()
    except:
        mac = "00:00:00:00:00:00"
    return mac[0:17]

for nif in nifs:
  if nif != "lo" and nif :
    deviceId = f'{getmac(nif)}-{sensId}'
    break

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      broker = sys.argv[pos]
    if pos == 2 :
      topic =  sys.argv[pos]
    if pos == 3 :
       udp = int(sys.argv[pos])
    if pos == 4 :
      t = sys.argv[pos]
      if t == "raw" or t == "data" or t == "none" :
         type = t
      else :
         print(f"Warning: wrong data type '{t}': use data or raw or none. Set to default '{type}'")
    if pos == 5 :
      t = sys.argv[pos]
      if t == "udp" or  t == "fake" :
        proto = t
      else :
        print(f"Warning: wrong proto mode '{t}': use udp or fake. Set to default '{proto}'")
    if pos == 6 :
       deviceId = sys.argv[pos]
    pos = pos + 1

client_id = deviceId

print (f"MQTT broker: '{broker}' topic '{topic}' deviceID '{client_id}' udp {udp} type '{type}' proto '{proto}'")

# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)

    # for never paho-mqtt >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)

    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def publish(client):
  msg_count = 1
  speed = 0.0
  global alt
  global lat
  global lon

  if proto == 'udp' :
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('', udp))

    while True:
      try:
        ready = False
        obj = time.gmtime(0)
        epoch = time.asctime(obj)
        curr_time = round(time.time()*1000)

        msg, address = server_socket.recvfrom(1024)
        raw = msg.decode("utf-8")
 
        pl = {}
        pl['ver'] = 1.0
        pl['app'] =  sys.argv[0]
        pl['deviceid'] = deviceId
        pl['ts'] = curr_time
        pl['status'] = 'on'
        pl['sensor'] = 'geo'

        if type == "none" :
            pl['type'] = 'none'
            nmea = 'none'
            ready = True

        if type == 'raw' :
          if "RMC" or "GLL" or "GGA" or "VTG" in raw:
            pl['type'] = 'raw'
            pl['raw']  = raw
            nmea = 'gps'
            ready = True

        if type ==  'data' :
           pl['type'] = 'data'
           # interested in GPRMC, GPGLL, GPVTG, GPGGA ref.:
           # https://www.rfwireless-world.com/Terminology/GPS-sentences-or-NMEA-sentences.html
           # lat,lon:      $GPGLL, 3723.2475, N, 12158.3416, W, 161229.487, A, A*41
           # lat,lon,spee: $GPRMC, 161229.487, A, 3723.2475, N, 12158.3416, W, 0.13, 309.62, 120598, , *10
           # lat,lon,alt:  $GPGGA, 161229.487, 3723.2475, N, 12158.3416, W, 1, 07, 1.0, 9.0, M, , , , 0000*18
           # speed:        $GPVTG, 309.62, T, ,M, 0.13, N, 0.2, K, A*23
           # note 1 Knot = 1.852 km per hour
           # "data": {"lat": 53.424461, "lon":14.749597, "alt": 200.2, "speed":0.0, "units": "kmH"},

           if "RMC" or "GLL" or "GGA" or "VTG" in raw:
             words = raw.split('$')
             if len(words) > 1 :
               if "GPRMC" or "GLRMC" in words[1]:
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLRMC" or nmea == "GPRMC" :
                   if len(w[3]) > 0 :
                     lat = float(w[3])
                   if len(w[5]) > 0 :
                     lon = float(w[5])
                   if len(w[7]) > 0 :
                     speed = float(w[7]) * 1.852
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   dt['alt'] = alt
                   pl['data'] =  dt
                   ready = True

               if "GPVTG" or "GLVTG" in words[1]:
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GPVTG" or nmea == "GLVTG" :
                   if len(w[7]) > 0:
                     speed = float(w[7])
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['alt'] = alt
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   pl['data'] =  dt
                   ready = True

               if "GPGLL" or "GLGLL" in words[1] :
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLGLL" or nmea == "GPGLL" :
                   if len(w[1]) > 0 :
                     lat = float(w[1])
                   if len(w[3]) > 0 :
                     lon = float(w[3])
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['alt'] = alt 
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   pl['data'] =  dt
                   ready = True

               if "GPGGA" or "GLGGA" in words[1] :
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLGGA" or nmea == "GPGGA" :
                   if len(w[2]) > 0 :
                     lat = float(w[2])
                   if len(w[4]) > 0 :
                     lon = float(w[4])
                   if len(w[9]) > 0 :
                     a = float(w[9])
                     if a > 0.01 or a < -0.01 :
                       alt = a
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   dt['alt'] = alt
                   pl['data'] =  dt
                   ready = True

        msg = json.dumps(pl)
        if ready:
          ready = False
          result = client.publish(topic, msg)
          status = result[0]
          if status == 0:
            print(f"\nReceived:\n{raw} from {address} parsed '{nmea}' and sent {msg_count} to topic '{topic}':\n{msg}")
          else:
            print(f"Failed to send message to topic {topic}")
          msg_count += 1

      except KeyboardInterrupt:
        print()
        sys.exit(0)

  if proto == 'fake' :
    while True:
      try:
        time.sleep(1)
        ready = False
        obj = time.gmtime(0)
        epoch = time.asctime(obj)
        curr_time = round(time.time()*1000)
        address = proto
       
        # take NMEA sentence using random index
        idx = random.randint(0, 3)        
        raws = list()
        raws.append('$GPGLL,5342.4461,N,01474.9595,E,215830.000,V,N*4E.')
        raws.append('$GPGGA,215830.000,5342.4461,N,01474.9595,E,0,00,127.000,417.593,M,0,M,,*75.')
        raws.append('$GPRMC,215830.000,V,5342.4461,N,01474.9595,E,0.000,248.929,050424,,E,N*37.')
        raws.append('$GPVTG,248.929,T,,M,0.000,N,0.000,K,N*0E.')
        raw = raws [idx]
        pl = {}
        pl['ver'] = 1.0
        pl['app'] =  sys.argv[0]
        pl['deviceid'] = deviceId
        pl['ts'] = curr_time
        pl['status'] = 'on'
        pl['sensor'] = 'geo'

        if type == "none" :
            pl['type'] = 'none'
            nmea = 'none'
            ready = True

        if type == 'raw' :
          if "RMC" or "GLL" or "GGA" or "VTG" in raw:
            pl['type'] = 'raw'
            pl['raw']  = raw
            nmea = 'gps'
            ready = True

        if type ==  'data' :
           pl['type'] = 'data'
           if "RMC" or "GLL" or "GGA" or "VTG" in raw:
             words = raw.split('$')
             if len(words) > 1 :
               if "GPRMC" or "GLRMC" in words[1]:
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLRMC" or nmea == "GPRMC" :
                   if len(w[3]) > 0 :
                     lat = float(w[3])
                   if len(w[5]) > 0 :
                     lon = float(w[5])
                   if len(w[7]) > 0 :
                     speed = float(w[7]) * 1.852
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   dt['alt'] = alt
                   pl['data'] =  dt
                   ready = True

               if "GPVTG" or "GLVTG" in words[1]:
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GPVTG" or nmea == "GLVTG" :
                   if len(w[7]) > 0:
                     speed = float(w[7])
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['alt'] = alt
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   pl['data'] =  dt
                   ready = True

               if "GPGLL" or "GLGLL" in words[1] :
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLGLL" or nmea == "GPGLL" :
                   if len(w[1]) > 0 :
                     lat = float(w[1])
                   if len(w[3]) > 0 :
                     lon = float(w[3])
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['alt'] = alt 
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   pl['data'] =  dt
                   ready = True

               if "GPGGA" or "GLGGA" in words[1] :
                 w = words[1].split(',')
                 nmea = w[0]
                 if nmea == "GLGGA" or nmea == "GPGGA" :
                   if len(w[2]) > 0 :
                     lat = float(w[2])
                   if len(w[4]) > 0 :
                     lon = float(w[4])
                   if len(w[9]) > 0 :
                     a = float(w[9])
                     if a > 0.01 or a < -0.01 :
                       alt = a
                   dt = {}
                   dt['lat'] = lat
                   dt['lon'] = lon
                   dt['speed'] = speed
                   dt['units'] = 'kmH'
                   dt['alt'] = alt
                   pl['data'] =  dt
                   ready = True

        msg = json.dumps(pl)
        if ready:
          ready = False
          result = client.publish(topic, msg)
          # result: [0, 1]
          status = result[0]
          if status == 0:
            print(f"\nReceived:\n{raw} from {address} parsed '{nmea}' and sent {msg_count} to topic '{topic}':\n{msg}")
          else:
            print(f"Failed to send message to topic {topic}")
          msg_count += 1

      except KeyboardInterrupt:
        print()
        sys.exit(0)

if __name__ == '__main__':
    # for paho-mqtt  version >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)
    client = connect_mqtt()
    client.loop_start()
    publish(client)
    client.loop_stop()
